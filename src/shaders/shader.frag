#version 450

layout(set = 0, binding = 0) uniform timeBuf {
  float elapsed_s;
} u_time;

layout(set = 0, binding = 1) uniform winBuf {
  vec2 size;
} u_window;

layout(location = 0) in vec3 i_color;

layout(location = 0) out vec4 f_color;

void main() {
  // The coordinates go from 0 to u_window.size
  // so we resize them to be between -1 and 1.
  // First we send them to the range 0..1
  vec2 uv = gl_FragCoord.xy / u_window.size;
  // then to -1..1
  uv -= vec2(0.5);
  uv *= 2;
  // The coordinates are now between -1 and 1,
  // but this may cause stretching if the resolution is not 1:1.
  // This may be desirable in some applications,
  // but here we correct for it
  uv.x *= max(u_window.size.x, u_window.size.y) / min(u_window.size.x, u_window.size.y);

  f_color = vec4(uv, (sin(u_time.elapsed_s) + 1) * 0.5 * 0.25, 1.0);
}
