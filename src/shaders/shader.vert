#version 450

layout(set = 0, binding = 0) uniform timeBuf {
  float elapsed_s;
} u_time;

layout(set = 0, binding = 1) uniform winBuf {
  vec2 size;
} u_window;

layout(location = 0) in vec2 position;
layout(location = 1) in vec3 color;

layout(location = 0) out vec3 o_color;

void main() {
  vec2 uv = position.xy;

  // Resolution remapping to avoid stretching
  if (u_window.size.x > u_window.size.y) {
    uv.x /= u_window.size.x / u_window.size.y;
  } else if (u_window.size.y > u_window.size.x) {
    uv.y /= u_window.size.y / u_window.size.x;
  }
  gl_Position = vec4(uv, 0.0, 1.0);
  o_color = color;
}